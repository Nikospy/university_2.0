package com.dsw.mikolaj.university.entity;

import java.util.Objects;
import java.util.Scanner;

public class Person {
    private String name;
    private String surname;
    private Gender gender;
    private Integer age;

    Person(String name, String surname, Integer age) {

        this.name = name;
        this.surname = surname;
        this.gender = setAutoGender(name);
        this.age = age;
    }

    Person() {
        System.out.print("Enter your's first name\n");
        this.name = new Scanner(System.in).nextLine();
        System.out.print("Enter your's last name\n");
        this.surname = new Scanner(System.in).nextLine();
        System.out.println("Enter your's gender, M for male, F for female");
        String gender = new Scanner(System.in).nextLine();
        if (gender.toUpperCase().startsWith("M")) {
            this.gender = Gender.Man;
        } else {
            this.gender = Gender.Female;
        }
        System.out.print("Enter your's age\n");
        int ageEntered = new Scanner(System.in).nextInt();
        boolean isAge = false;
        while (!isAge) {
            try {
                if (ageEntered < 18) {
                    throw new AgeException();
                }
            } catch (AgeException e) {
                e.printStackTrace();
            } finally {
                if (ageEntered >= 18) {
                    isAge = true;
                    this.age = ageEntered;
                } else {
                    System.out.print("Enter your's correct age\n");
                    ageEntered = new Scanner(System.in).nextInt();

                }
            }
        }
    }

    @Override
    public String toString() {
        return
                name + " " + surname + " | " +
                        gender + " | Age: " + age + " | ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname) &&
                gender == person.gender &&
                Objects.equals(age, person.age);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, surname, gender, age);
    }

    private Gender setAutoGender(String name) {
        return name.endsWith("a") ? Gender.Female : Gender.Man;
    }

    String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }


}
