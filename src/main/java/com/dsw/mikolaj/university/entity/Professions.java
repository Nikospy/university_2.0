package com.dsw.mikolaj.university.entity;

import com.google.common.collect.Lists;

import java.util.List;

public class Professions {
    static List<String> professions = Lists.newArrayList("Lecturer", "Cleaner", "Porter", "Secretary", "Headmaster", "Librarian", "IT specialist");

    @Override
    public String toString() {
        return "Professions{" +
                "professions=" + professions +
                '}';
    }

    List<String> getProfessions() {
        return professions;
    }

    public void deleteProfession(String profession){
        professions.remove(profession);
    }

    public void setProfessions(List<String> professions) {
        Professions.professions = professions;
    }
}
