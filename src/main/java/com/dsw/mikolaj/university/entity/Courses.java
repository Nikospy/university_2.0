package com.dsw.mikolaj.university.entity;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

public class Courses {
    static List<String> allCourses = Lists.newArrayList("Mathematics", "History", "Computer Science", "Law", "Physics", "Art");
    static Map<String, List<String>> courses = Maps.newHashMap();

    {
        courses.put("Mathematics", Subjects.getMathSubjects());
        courses.put("History", Subjects.getHistorySubjects());
        courses.put("Computer Science", Subjects.getCsSubjects());
        courses.put("Law", Subjects.getLawSubjects());
        courses.put("Physics", Subjects.getPhysicsSubjects());
        courses.put("Art", Subjects.getArtSubjects());
    }

    @Override
    public String toString() {
        return "Courses{" +
                ", courses=" + courses.keySet() +
                '}';
    }

    Map<String, List<String>> getCourses() {
        return courses;
    }

    public void deleteCourse(String profession) {
        courses.remove(profession);
    }

    public void addCourse(String course, List<String> subjects) {
        courses.put(course, subjects);
    }

}
