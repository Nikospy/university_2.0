package com.dsw.mikolaj.university.entity;

public enum Gender {
    Man(0),Female(1);

    private int numValue;

    Gender (int numValue){this.numValue=numValue;}

    public int getNumValue(){
        return numValue;
    }
}
