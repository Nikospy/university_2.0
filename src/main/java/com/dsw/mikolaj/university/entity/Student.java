package com.dsw.mikolaj.university.entity;

import com.google.common.collect.Maps;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.dsw.mikolaj.university.entity.Deanery.students;

public class Student extends Person {
    private Deanery d = new Deanery();
    private Courses c = new Courses();
    private Map<String, List<String>> courses = c.getCourses();
    private Map<String, List<String>> courseOfStudent;
    private Map<String, List<Integer>> degrees;
    private String studentID;
    private static int id = 1000;
    private GradeYear gradeYear;

    Student(String name, String surname, Integer age, GradeYear gradeYear, String studentCourse) {
        super(name, surname, age);
        this.gradeYear = gradeYear;
        boolean contains = false;
        Map<String, List<String>> actualCourse = Maps.newHashMap();
        Map<String, List<Integer>> actualDegrees = Maps.newHashMap();
        while (!contains) {
            if (!courses.containsKey(studentCourse)) {
                System.out.println("Write course which is available below");
                System.out.println(c.getCourses());
            } else {
                for (Map.Entry entry : courses.entrySet()) {
                    if (entry.getKey().toString().equals(studentCourse)) {
                        actualCourse.put(entry.getKey().toString(), Collections.singletonList(entry.getValue().toString()));
                        setCourseOfStudent(actualCourse);
                        List<List<String>> arraySubjects = courseOfStudent.values().stream().collect(Collectors.toList());

                        String subjectsAsOneString = String.valueOf(arraySubjects);
                        List<String> listSubjects = new ArrayList<>(Arrays.asList((subjectsAsOneString.split("(?=\\p{Upper})|\\,|\\]|\\["))));
                        listSubjects.removeAll(Collections.singleton(""));
                        listSubjects.removeAll(Collections.singleton(" "));

                        for (int i = 0; i < listSubjects.size(); i++) {
                            actualDegrees.put(listSubjects.get(i), d.randomDegrees());
                        }
                        setDegrees(actualDegrees);
                        contains = true;
                    }
                }
            }
        }
        setStudentID();
        students.add(this);
    }

    Student() {
        super();
        System.out.print("Enter student's year of study\n");
        int year = new Scanner(System.in).nextInt();
        boolean isYear = false;
        while (!isYear) {
            int finalYear = year;
            if (!Stream.of(GradeYear.values()).anyMatch(gradeYear1 -> gradeYear1.getNumValue() == finalYear)) {
                System.out.println("Study doesn't last so long\nEnter student's year of study");
                year = new Scanner(System.in).nextInt();
            } else {
                for (GradeYear g : GradeYear.values()) {
                    if (g.getNumValue() == year) {
                        this.gradeYear = g;
                        isYear = true;
                    }
                }
            }
        }
        System.out.println("Here is the lists of available courses");
        System.out.println(c.getCourses());

        System.out.println("Enter the name of course you want to apply");
        String course = new Scanner(System.in).nextLine();
        boolean contains = false;
        Map<String, List<String>> actualCourse = Maps.newHashMap();
        Map<String, List<Integer>> actualDegrees = Maps.newHashMap();
        while (!contains) {
            if (!courses.containsKey(course)) {
                System.out.println("Write course which is available below");
                System.out.println(c.getCourses());
                course = new Scanner(System.in).nextLine();
                System.out.println(course);
            } else {
                for (Map.Entry entry : courses.entrySet()) {
                    if (entry.getKey().toString().equals(course)) {
                        actualCourse.put(entry.getKey().toString(), Collections.singletonList(entry.getValue().toString()));
                        setCourseOfStudent(actualCourse);
                        List<List<String>> arraySubjects = courseOfStudent.values().stream().collect(Collectors.toList());

                        String subjectsAsOneString = String.valueOf(arraySubjects);
                        List<String> listSubjects = new ArrayList<>(Arrays.asList((subjectsAsOneString.split(",|\\[|\\]"))));
                        listSubjects.removeAll(Collections.singleton(""));

                        for (int i = 0; i < listSubjects.size(); i++) {
                            actualDegrees.put(listSubjects.get(i), d.randomDegrees());
                        }
                        setDegrees(actualDegrees);
                        contains = true;
                    }
                }
            }
        }
        setCourseOfStudent(actualCourse);
        setStudentID();
        students.add(this);
    }

    GradeYear getGradeYear() {
        return gradeYear;
    }

    private void setStudentID() {
        id++;
        this.studentID = this.gradeYear.getNumValue() + "" + id;
    }

    String getStudentID() {
        return studentID;
    }

    @Override
    public String toString() {
        return super.toString() +
                gradeYear + " Year"+
                " | ID: " + studentID  +
                " | Course: " + getNameOfCourse() +
                " | Degrees: " + getDegrees();
    }

    private void setCourseOfStudent(Map<String, List<String>> courseOfStudent) {
        this.courseOfStudent = courseOfStudent;
    }

    private void setDegrees(Map<String, List<Integer>> actualDegrees) {
        this.degrees = actualDegrees;
    }

    Map<String, List<String>> getCourseOfStudent() {
        return courseOfStudent;
    }

    Map<String, List<Integer>> getDegrees() {
        return degrees;
    }

    private String getNameOfCourse() {
        String nameOfCourse = null;
        for (Map.Entry entry : courseOfStudent.entrySet()) {
            nameOfCourse = String.valueOf(entry.getKey());
        }
        return nameOfCourse;
    }

}
