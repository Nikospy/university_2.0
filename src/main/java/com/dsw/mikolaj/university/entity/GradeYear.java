package com.dsw.mikolaj.university.entity;

public enum GradeYear {
    First(1),Second(2),Third(3),Fourth(4),Fifth(5);

    private int numValue;

    GradeYear(int numValue) {
        this.numValue = numValue;
    }

    public int getNumValue(){
        return numValue;
    }
}
