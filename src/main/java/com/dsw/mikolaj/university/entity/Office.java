package com.dsw.mikolaj.university.entity;

import com.github.javafaker.Faker;
import com.google.common.collect.Lists;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Office  {
    private Faker faker = new Faker(new Locale("pl"));
    public static List<Employee> employees = Lists.newArrayList();

    private String randomName(){
        return faker.name().firstName();
    }

    private String randomSurname(){
        return faker.name().lastName();
    }

    private int randomAge(){
        return ThreadLocalRandom.current().nextInt(18,60);
    }

    private int randomIntOfWork(){
        return ThreadLocalRandom.current().nextInt(1,45);
    }

    private int randomSalary(){
        int salary = ThreadLocalRandom.current().nextInt(2000,5000);
        salary-=salary%10;
        salary-=salary%100;
        return (int)(Math.round( salary / 10.0) * 10);
    }

    public Gender randomGender(){
        return Gender.values()[new Random().nextInt(Gender.values().length)];
    }

    private String randomProfession(){
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(Professions.professions.size());
        return Professions.professions.get(index);
    }

    public void randomEmployee(){
        for (int i = 0; i < 100; i++) {
            new Employee(randomName(),randomSurname(),randomAge(),randomIntOfWork(),randomSalary(),randomProfession());
        }
    }

    public void sortedProfession(){
        List<String> sortedProfession = Professions.professions.stream()
                .sorted()
                .collect(Collectors.toList());
        for (int i = 0; i < sortedProfession.size(); i++) {
            if (i % 4 != 0) {
                System.out.print(sortedProfession.get(i) + " | ");
            } else {
                System.out.print("\n" + sortedProfession.get(i) + " | ");
            }
        }
    }

    public long howManyEmployees(){
        return employees.stream().count();
    }

    public void printEmployees() {
        for (Employee e : employees) {
            System.out.println(e);
        }
    }

    public void setBonus() {
        for (Employee employee : employees) {
            if (employee.getYearsOfWork() >= 3 && employee.getYearsOfWork() <= 5) {
                int salary = employee.getSalary() + 300;
                employee.setSalary(salary);
            } else if (employee.getYearsOfWork() > 6 && employee.getYearsOfWork() <= 10) {
                int salary = employee.getSalary() + 500;
                employee.setSalary(salary);
            } else if (employee.getYearsOfWork() > 10) {
                int salary = employee.getSalary() + 800;
                employee.setSalary(salary);
            }

        }
        System.out.println("Bonus has been set");
    }

    public void createEmployee() {
        new Employee();
    }

    public void addProfession() {
        System.out.println("Please enter the name of profession you want to add");
        String profession = new Scanner(System.in).nextLine();
        Professions.professions.add(profession);
    }

    public Map<Gender, Long> howManyEmployeesGender(){
        Map<Gender, Long> counted = employees.stream()
                .collect(Collectors.groupingBy(Employee::getGender,
                        Collectors.counting()));
        return counted;
    }

    public void deleteEmployee() {
        System.out.println("Enter employee's ID to remove");
        String id = new Scanner(System.in).nextLine();
        if (employees.removeIf(employee -> employee.getEmployeeID().equals(id))) {
            System.out.println("Employee has been removed");
        } else {
            System.out.println("ID didn't match to any employee");
        }
    }

}
