package com.dsw.mikolaj.university.entity;

public class AgeException extends Throwable {
    public AgeException(){
        super("The age you have entered is not valid");
    }
}
