package com.dsw.mikolaj.university.entity;

import com.google.common.collect.Lists;

import java.util.List;

public class Subjects {
    private static List<String> mathSubjects = Lists.newArrayList("Algebra", "Mathematical analysis", "Statistics");
    private static List<String> historySubjects = Lists.newArrayList("Ancient history", "Latin", "Archeology");
    private static List<String> csSubjects = Lists.newArrayList("Basics of programming", "Algorithms", "IT systems");
    private static List<String> lawSubjects = Lists.newArrayList("Criminal law", "Law logic", "Administration");
    private static List<String> physicsSubjects = Lists.newArrayList("Astrophysics", "Experimental physics", "Nanotechnology");
    private static List<String> artSubjects = Lists.newArrayList("History of art", "Painting", "Modern architecture");
    private static List<String> allSubjects = Lists.newArrayList();

    static {
        allSubjects.addAll(mathSubjects);
        allSubjects.addAll(historySubjects);
        allSubjects.addAll(csSubjects);
        allSubjects.addAll(lawSubjects);
        allSubjects.addAll(physicsSubjects);
        allSubjects.addAll(artSubjects);
    }

    @Override
    public String toString() {
        return "Subjects{" +
                "mathSubjects=" + mathSubjects +
                ", historySubjects=" + historySubjects +
                ", csSubjects=" + csSubjects +
                ", lawSubjects=" + lawSubjects +
                ", physicsSubjects=" + physicsSubjects +
                ", artSubjects=" + artSubjects +
                '}';
    }


    static List<String> getMathSubjects() {
        return mathSubjects;
    }

    public void setMathSubjects(List<String> mathSubjects) {
        Subjects.mathSubjects = mathSubjects;
    }

    static List<String> getHistorySubjects() {
        return historySubjects;
    }

    public void setHistorySubjects(List<String> historySubjects) {
        Subjects.historySubjects = historySubjects;
    }

    static List<String> getCsSubjects() {
        return csSubjects;
    }

    public void setCsSubjects(List<String> csSubjects) {
        Subjects.csSubjects = csSubjects;
    }

    static List<String> getLawSubjects() {
        return lawSubjects;
    }

    public void setLawSubjects(List<String> lawSubjects) {
        Subjects.lawSubjects = lawSubjects;
    }

    static List<String> getPhysicsSubjects() {
        return physicsSubjects;
    }

    public void setPhysicsSubjects(List<String> physicsSubjects) {
        Subjects.physicsSubjects = physicsSubjects;
    }

    static List<String> getArtSubjects() {
        return artSubjects;
    }

    public void setArtSubjects(List<String> artSubjects) {
        Subjects.artSubjects = artSubjects;
    }

    static List<String> getAllSubjects() {
        return allSubjects;
    }

    public static void setAllSubjects(List<String> allSubjects) {
        Subjects.allSubjects = allSubjects;
    }
}
