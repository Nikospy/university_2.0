package com.dsw.mikolaj.university.entity;

import com.github.javafaker.Faker;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Deanery {

    // library for generating fake data
    private Faker faker = new Faker(new Locale("pl"));
    public static List<Student> students = Lists.newArrayList();


    private String randomCourse() {
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(Courses.allCourses.size());
        return Courses.allCourses.get(index);
    }

    private int randomAge() {
        return ThreadLocalRandom.current().nextInt(18, 60);
    }

    public Gender randomGender() {
        return Gender.values()[new Random().nextInt(Gender.values().length)];
    }

    private GradeYear randomGradeYear() {
        return GradeYear.values()[new Random().nextInt(GradeYear.values().length)];
    }

    private String randomName() {
        return faker.name().firstName();
    }

    private String randomSurname() {
        return faker.name().lastName();
    }

    public void randomStudent() {
        for (int i = 0; i < 100; i++) {
            new Student(randomName(), randomSurname(), randomAge(), randomGradeYear(), randomCourse());
        }
    }

    public void searchStudentBySubstring() {
        System.out.println("Enter some characters of surname");
        String substring = new Scanner(System.in).nextLine().toLowerCase();
        List<Student> filtList = students.stream()
                .filter(x -> x.getSurname().toLowerCase().startsWith(substring))
                .collect(Collectors.toList());

        for (Student student : filtList) {
            System.out.println("* " + student.getName() + " " + student.getSurname());
        }
    }

    public long howManyStudents() {
        return students.stream().count();
    }

    public Map<GradeYear, Long> howManyStudentsOnYear() {
        Map<GradeYear, Long> counted = students.stream()
                .collect(Collectors.groupingBy(Student::getGradeYear,
                        Collectors.counting()));
        return counted;
    }

    public Map<Gender, Long> howManyStudentsGender() {
        Map<Gender, Long> counted = students.stream()
                .collect(Collectors.groupingBy(Student::getGender,
                        Collectors.counting()));
        return counted;
    }

    public void printStudents() {
        for (Student s : students) {
            System.out.println(s);
        }
    }

    public void sortedSubjects() {
        List<String> sortedSubjects = Subjects.getAllSubjects().stream()
                .sorted()
                .collect(Collectors.toList());
        for (int i = 0; i < sortedSubjects.size(); i++) {
            if (i % 4 != 0) {
                System.out.print(sortedSubjects.get(i) + " | ");
            } else {
                System.out.print("\n" + sortedSubjects.get(i) + " | ");
            }
        }
    }

    public void sortedCourses() {
        List<String> sortedCourses = Courses.allCourses.stream()
                .sorted()
                .collect(Collectors.toList());
        for (int i = 0; i < sortedCourses.size(); i++) {
            if (i % 4 != 0) {
                System.out.print(sortedCourses.get(i) + " | ");
            } else {
                System.out.print("\n" + sortedCourses.get(i) + " | ");
            }
        }
    }

    public void joinedSujects() {
        String joinedSubjects = Subjects.getAllSubjects().stream()
                .collect(Collectors.joining(","));
        System.out.println(joinedSubjects);
    }

    public void sortedNameSurname() {
        List<Student> sortedNameSurname = students.stream().sorted(Comparator.comparing(Student::getName).thenComparing(Student::getSurname))
                .collect(Collectors.toList());
        for (Student student : sortedNameSurname) {
            System.out.println("* " + student.getName() + " " + student.getSurname());
        }
    }

    public void sortedByGradeYear() {
        List<Student> sortedByGradeYear = students.stream().sorted(Comparator.comparing(Student::getGradeYear)).collect(Collectors.toList());
        for (Student student : sortedByGradeYear) {
            System.out.println("* " + student.getName() + " " + student.getSurname() + ": " + student.getGradeYear());
        }
    }

    public void addDegree() {
        for (Student s : students) {
            System.out.printf("%s %s: %s\n", s.getName(), s.getSurname(), s.getStudentID());
        }
        System.out.println("Enter student's ID to whom you add degree");
        String id = new Scanner(System.in).nextLine();

        for (Student student : students) {
            if (student.getStudentID().equals(id)) {
                System.out.println(student.getCourseOfStudent());
                System.out.println("Enter subject to which you want to add degree");
                String subject = new Scanner(System.in).nextLine();
                for (Map.Entry entry : student.getDegrees().entrySet()) {
                    if (entry.getKey().toString().contains(subject)) {
                        System.out.println("Give the degree to be entered");
                        int degree = new Scanner(System.in).nextInt();
                        boolean isDegree = false;
                        while (!isDegree) {
                            int finalDegree = degree;
                            if (finalDegree >= 2 && finalDegree <= 5) {
                                student.getDegrees().get(subject).add(finalDegree);
                                System.out.println(student.getDegrees().get(subject));
                                isDegree = true;
                            } else {
                                System.out.println("Enter degree from the correct range (2-5)");
                                degree = new Scanner(System.in).nextInt();
                            }
                        }
                    }
                }
            }
        }
    }

    public void allStudentsAverageDegrees() {
        DecimalFormat df2 = new DecimalFormat("#.##");
        Integer integerSum = 0;
        double integerCount = 0;
        for (Student s : students) {
            integerSum += s.getDegrees().values().stream()
                    .flatMapToInt(list -> list.stream().mapToInt(Integer::intValue))
                    .sum();
            integerCount += s.getDegrees().values().stream()
                    .flatMapToInt(list -> list.stream().mapToInt(Integer::intValue))
                    .count();
        }
        double average = integerSum / integerCount;
        System.out.println("Average of all students: " + df2.format(average));
    }

    public void studentAverageDegree() {
        DecimalFormat df2 = new DecimalFormat("#.##");
        Integer integerSum = 0;
        double integerCount = 0;
        for (Student s : students) {
            System.out.printf("%s %s: %s\n", s.getName(), s.getSurname(), s.getStudentID());
        }
        System.out.println("Enter student ID to display it's average");
        String id = new Scanner(System.in).nextLine();
        for (Student s : students) {
            if (s.getStudentID().equals(id)) {
                integerSum += s.getDegrees().values().stream()
                        .flatMapToInt(list -> list.stream().mapToInt(Integer::intValue))
                        .sum();
                integerCount += s.getDegrees().values().stream()
                        .flatMapToInt(list -> list.stream().mapToInt(Integer::intValue))
                        .count();
            }
        }
        double average = integerSum / integerCount;
        System.out.println("Average of this student: " + df2.format(average));
    }

    public void bestAverageDegree() {
        DecimalFormat df2 = new DecimalFormat("#.##");
        Integer integerSum = 0;
        double integerCount = 0;
        Map<String, Double> bestAverageStudent = Maps.newHashMap();
        for (Student s : students) {
            integerSum += s.getDegrees().values().stream()
                    .flatMapToInt(list -> list.stream().mapToInt(Integer::intValue))
                    .sum();
            integerCount += s.getDegrees().values().stream()
                    .flatMapToInt(list -> list.stream().mapToInt(Integer::intValue))
                    .count();

            double average = integerSum / integerCount;

            bestAverageStudent.put(s.getName() + " " + s.getSurname(), average);
        }
        Double max = bestAverageStudent.values().stream().max(Comparator.naturalOrder()).get();
        Map.Entry<String, Double> maxList = bestAverageStudent.entrySet().stream()
                .max(Map.Entry.comparingByValue()).get();

        System.out.println("* " + maxList.getKey() + ": " + df2.format(maxList.getValue()));
    }

    public void studentSubjectAverageDegree() {
        DecimalFormat df2 = new DecimalFormat("#.##");
        Integer integerSum = 0;
        double integerCount = 0;
        for (Student s : students) {
            System.out.printf("%s %s: %s\n", s.getName(), s.getSurname(), s.getStudentID());
        }
        System.out.println("Enter student ID to display it's average");
        String id = new Scanner(System.in).nextLine();
        for (Student s : students) {
            if (s.getStudentID().equals(id)) {
                System.out.println(s.getCourseOfStudent());
                System.out.println("Enter subject to which you want to see average");
                String subject = new Scanner(System.in).nextLine();
                for (Map.Entry entry : s.getDegrees().entrySet()) {
                    if (entry.getKey().equals(subject)) {
                        integerSum += s.getDegrees().get(subject).stream()
                                .flatMapToInt(list -> IntStream.of(list))
                                .sum();
                        integerCount += s.getDegrees().get(subject).stream()
                                .flatMapToInt(list -> IntStream.of(list))
                                .count();
                        double average = integerSum / integerCount;
                        System.out.println("Average of this student subject: " + df2.format(average));
                    }
                }
            }
        }
    }

    public int randomInt() {
        Random rand = new Random();
        int randomNum = rand.nextInt(4);
        return randomNum + 2;
    }

    public List<Integer> randomDegrees() {
        List<Integer> degrees = new ArrayList<>(Arrays.asList());
        for (int i = 0; i < 6; i++) {
            degrees.add(i, randomInt());
        }
        return degrees;
    }

    public void createStudent() {
        new Student();
    }

    public void deleteStudent() {
        System.out.println("Enter student's ID to remove");
        String id = new Scanner(System.in).nextLine();
        if (students.removeIf(student -> student.getStudentID().equals(id))) {
            System.out.println("Student has been removed");
        } else {
            System.out.println("ID didn't match to any student");
        }
    }

    public void addCourse() {
        List<String> subjects = Lists.newArrayList();
        boolean is = false;
        System.out.println("Please enter the name of course you want to add");
        String course = new Scanner(System.in).nextLine();
        while (!is) {
            System.out.println("Please enter the name of subjects of that course");
            String subject = new Scanner(System.in).nextLine();
            subjects.add(subject);
            System.out.println("Do you want to add another subject? Y for YES or N for NO");
            String answer = new Scanner(System.in).nextLine();
            if (answer.toUpperCase().equals("N")) {
                is = true;
            }
        }
        Courses.allCourses.add(course);
        Courses.courses.put(course, subjects);
    }

}
