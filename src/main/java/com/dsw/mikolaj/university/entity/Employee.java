package com.dsw.mikolaj.university.entity;


import java.util.Scanner;
import java.util.stream.Stream;

public class Employee extends Person  {
    private int yearsOfWork;
    private String profession;
    private Professions professions = new Professions();
    private String employeeID;



    private static int id = 9900;
    private int salary;

    Employee(String name, String surname, Integer age, int yearsOfWork, int salary, String profession) {
        super(name, surname, age);
        this.yearsOfWork = yearsOfWork;
        this.salary = salary;
        boolean contains = false;
        while (!contains) {
            String finalProfession = profession;
            if (Stream.of(professions.getProfessions()).anyMatch(x -> x.contains(finalProfession))) {
                contains = true;
            } else {
                System.out.println("Write profession which is available below");
                System.out.println(professions.getProfessions());
                profession = new Scanner(System.in).nextLine();
                System.out.println(profession);
            }
            this.profession = profession;
        }
        setEmployeeID();
        Office.employees.add(this);
    }


    Employee() {
        super();
        System.out.print("Enter employee's year of work\n");
        this.yearsOfWork = new Scanner(System.in).nextInt();
        System.out.print("Enter employee's salary\n");
        this.salary = new Scanner(System.in).nextInt();

        System.out.println("Here is the lists of available professions");
        System.out.println(professions.getProfessions());
        System.out.println("Enter the name of profession you want to apply");
        String profession = new Scanner(System.in).nextLine();
        boolean contains = false;
        while (!contains) {
            String finalProfession = profession;
            if (Stream.of(professions.getProfessions()).anyMatch(x -> x.contains(finalProfession))) {
                contains = true;
            } else {
                System.out.println("Write profession which is available below");
                System.out.println(professions.getProfessions());
                profession = new Scanner(System.in).nextLine();
                System.out.println(profession);
            }
            this.profession = profession;
        }
        setEmployeeID();
        Office.employees.add(this);
    }

    @Override
    public String toString() {
        return super.toString() + "Employee{" +
                "yearsOfWork=" + yearsOfWork +
                ", profession='" + profession + '\'' +
                ", employeeID='" + employeeID + '\'' +
                ", salary='" + getSalary() + '\'' +
                '}';
    }

    private void setEmployeeID() {
        id++;
        this.employeeID = yearsOfWork + "" + id;
    }


    int getYearsOfWork() {
        return yearsOfWork;
    }

    public String getProfessions() {
        return profession;
    }

    int getSalary() {
        return salary;
    }

    void setSalary(int salary) {
        this.salary = salary;
    }

    String getEmployeeID() {
        return employeeID;
    }
}

