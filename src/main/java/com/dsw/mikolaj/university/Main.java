package com.dsw.mikolaj.university;

import com.dsw.mikolaj.university.entity.Deanery;
import com.dsw.mikolaj.university.entity.Office;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {

        Deanery d = new Deanery();
        d.randomStudent();
        Office o = new Office();
        o.randomEmployee();
        boolean exit = false;
        int menuChoice;
        do {
            printMenu();
            menuChoice = new Scanner(System.in).nextInt();
            switch (menuChoice) {
                case 1:
                    d.printStudents();
                    printDots();
                    break;
                case 2:
                    o.printEmployees();
                    printDots();
                    break;
                case 3:
                    d.createStudent();
                    printDots();
                    break;
                case 4:
                    o.createEmployee();
                    printDots();
                    break;
                case 5:
                    d.deleteStudent();
                    printDots();
                    break;
                case 6:
                    o.deleteEmployee();
                    printDots();
                    break;
                case 7:
                    d.sortedSubjects();
                    printDots();
                    break;
                case 8:
                    d.sortedCourses();
                    printDots();
                    break;
                case 9:
                    d.sortedByGradeYear();
                    printDots();
                    break;
                case 10:
                    d.joinedSujects();
                    printDots();
                    break;
                case 11:
                    d.sortedNameSurname();
                    printDots();
                    break;
                case 12:
                    d.allStudentsAverageDegrees();
                    printDots();
                    break;
                case 13:
                    d.studentAverageDegree();
                    printDots();
                    break;
                case 14:
                    d.studentSubjectAverageDegree();
                    printDots();
                    break;
                case 15:
                    d.bestAverageDegree();
                    printDots();
                    break;
                case 16:
                    d.addDegree();
                    printDots();
                    break;
                case 17:
                    d.addCourse();
                    printDots();
                    break;
                case 18:
                    d.searchStudentBySubstring();
                    printDots();
                    break;
                case 19:
                    System.out.println(d.howManyStudents());
                    printDots();
                    break;
                case 20:
                    System.out.println(d.howManyStudentsOnYear());
                    printDots();
                    break;
                case 21:
                    System.out.println(d.howManyStudentsGender());
                    printDots();
                    break;
                case 22:
                    System.out.println(o.howManyEmployeesGender());
                    printDots();
                    break;
                case 23:
                    System.out.println(o.howManyEmployees());
                    printDots();
                    break;
                case 24:
                    o.setBonus();
                    printDots();
                    break;
                case 25:
                    o.addProfession();
                    printDots();
                    break;
                case 26:
                    o.sortedProfession();
                    printDots();
                    break;
                case 0:
                    exit = true;
                    break;
                default:
                    System.out.println("Invalid choice");
            }
        } while (!exit);
        System.out.println("Good bye!");
    }

    private static void printMenu() {
        System.out.println("\n***** MENU *****");
        System.out.println("1 - Show all students\n"
                + "2  - Show all employees\n"
                + "3  - Add student\n"
                + "4  - Add employee\n"
                + "5  - Delete student\n"
                + "6  - Delete employee\n"
                + "7  - Show sorted subjects\n"
                + "8  - Show sorted courses\n"
                + "9  - Show sorted students by grade year\n"
                + "10 - Show subjects joined as one string\n"
                + "11 - Show sorted students by name and surname\n"
                + "12 - Show average grade of all students\n"
                + "13 - Show average grade of particular students\n"
                + "14 - Show average grade of particular subjects of students\n"
                + "15 - Show average of best students\n"
                + "16 - Add grade to student\n"
                + "17 - Add course\n"
                + "18 - Search student by surname\n"
                + "19 - Show how many students is present\n"
                + "20 - Show how many students is present on grade year\n"
                + "21 - Show how many male and female in students\n"
                + "22 - Show how many male and female in employees\n"
                + "23 - Show how many employees is present\n"
                + "24 - Set bonus to employees\n"
                + "25 - Add profession\n"
                + "26 - Show sorted profession\n"
                + "0  - Exit");
        System.out.println("***** Enter particular number *****");
    }

    private static void printDots() throws IOException, InterruptedException {
        String anim = ".";
        for (int x = 0; x < 40; x++) {
            String data = "" + anim.charAt(x % anim.length());
            System.out.write(data.getBytes());
            Thread.sleep(75);
        }
    }
}


